package org.guice.demo.annotation.custom;

import org.guice.demo.annotation.custom.impl.FullCustomBar;
import org.testng.annotations.Test;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class RebBlueRunner {

    @Test
    public void test() {
        IBar bar = RedBlueModule.getInjector().getInstance(FullCustomBar.class);
        bar.print();
    }
}
