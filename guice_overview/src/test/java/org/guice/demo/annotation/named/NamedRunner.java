package org.guice.demo.annotation.named;

import org.testng.annotations.Test;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class NamedRunner {

    @Test
    public void test() {
        Doo doo = NamedModule.getInjector().getInstance(Doo.class);
        doo.print();
    }
}
