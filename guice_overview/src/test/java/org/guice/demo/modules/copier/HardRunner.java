package org.guice.demo.modules.copier;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.guice.demo.modules.copier.HardCopyModule;
import org.guice.demo.modules.copier.ICopier;
import org.testng.annotations.Test;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class HardRunner {

    @Test
    public void test() {
        Injector injector = Guice.createInjector(new HardCopyModule());
        ICopier service = injector.getInstance(ICopier.class);
        service.copy();
    }
}
