package org.guice.demo.modules.copier;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.testng.annotations.Test;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class SimpleRunner {

    @Test
    public void test() {
        Injector injector = Guice.createInjector(new SimpleCopyModule());
        ICopier service = injector.getInstance(ICopier.class);


        service.copy();
    }
}
