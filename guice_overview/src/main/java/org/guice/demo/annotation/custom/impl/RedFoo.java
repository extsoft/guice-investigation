package org.guice.demo.annotation.custom.impl;

import com.google.inject.Inject;
import org.guice.demo.annotation.custom.IBar;
import org.guice.demo.annotation.custom.IFoo;

/**
 * User: dserdyuk
 * Date: 05.11.13
 */
public class RedFoo implements IFoo {

    private final IBar iBar;

    @Inject
    public RedFoo(IBar iBar) {
        this.iBar = iBar;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    public IBar getBar() {
        return iBar;
    }
}
