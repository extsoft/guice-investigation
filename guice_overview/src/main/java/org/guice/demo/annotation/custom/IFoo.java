package org.guice.demo.annotation.custom;

/**
 * User: dserdyuk
 * Date: 05.11.13
 */
public interface IFoo {

    String getName();
}
