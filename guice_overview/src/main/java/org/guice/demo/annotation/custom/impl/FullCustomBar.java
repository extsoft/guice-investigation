package org.guice.demo.annotation.custom.impl;

import com.google.inject.Inject;
import org.guice.demo.annotation.custom.Blue;
import org.guice.demo.annotation.custom.IBar;
import org.guice.demo.annotation.custom.IFoo;
import org.guice.demo.annotation.custom.Red;

/**
 * User: dserdyuk
 * Date: 05.11.13
 */
public class FullCustomBar implements IBar {

    @Inject
    @Red
    IFoo rFoo;
    @Inject
    @Blue
    IFoo bFoo;
    @Inject
    IFoo foo;


    @Override
    public void print() {
        System.out.println(rFoo.getName());
        System.out.println(bFoo.getName());
        System.out.println(foo.getName());
    }
}
