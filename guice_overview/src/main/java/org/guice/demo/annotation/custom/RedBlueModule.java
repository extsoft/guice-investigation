package org.guice.demo.annotation.custom;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.guice.demo.annotation.custom.impl.BlueFoo;
import org.guice.demo.annotation.custom.impl.Foo;
import org.guice.demo.annotation.custom.impl.FullCustomBar;
import org.guice.demo.annotation.custom.impl.RedFoo;

/**
 * User: dserdyuk
 * Date: 05.11.13
 */
public class RedBlueModule extends AbstractModule {

    private static Injector injector;

    public static Injector getInjector() {
        if (injector == null) {
            injector = Guice.createInjector(new RedBlueModule());
        }
        return injector;
    }


    @Override
    protected void configure() {
        bind(IFoo.class).annotatedWith(Red.class).to(RedFoo.class);
        bind(IFoo.class).annotatedWith(Blue.class).to(BlueFoo.class);
        bind(IFoo.class).to(Foo.class);
        bind(IBar.class).to(FullCustomBar.class);
    }
}
