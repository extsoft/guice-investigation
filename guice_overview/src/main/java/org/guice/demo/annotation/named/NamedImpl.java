package org.guice.demo.annotation.named;

import java.lang.annotation.Annotation;

/**
 * User: dserdyuk
 * Date: 10.12.13
 */
public class NamedImpl implements Named {

    final String value;

    public NamedImpl(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    public int hashCode() {
        return 127 * "value".hashCode() ^ value.hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Named))
            return false;
        Named other = (Named) o;
        return value.equals(other.value());
    }

    public String toString() {
        return "@" + Named.class.getName() + "(value=" + value + ")";
    }

    public Class<? extends Annotation> annotationType() {
        return Named.class;
    }
}
