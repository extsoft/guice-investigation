package org.guice.demo.annotation.named;

import com.google.inject.Inject;

/**
 * User: dserdyuk
 * Date: 10.12.13
 */
public class Doo {

    @Inject
    @Named("s")
    Goo gooS;
    @Inject
    @Named("f")
    Goo gooF;

    public void print() {
        System.out.println(gooF.getGoo());
        System.out.println(gooS.getGoo());
    }
}
