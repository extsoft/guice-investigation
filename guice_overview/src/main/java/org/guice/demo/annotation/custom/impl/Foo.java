package org.guice.demo.annotation.custom.impl;

import com.google.inject.Inject;
import org.guice.demo.annotation.custom.IBar;
import org.guice.demo.annotation.custom.IFoo;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class Foo implements IFoo {

    private final IBar iBar;

    @Inject
    public Foo(IBar iBar) {
        this.iBar = iBar;
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
}
