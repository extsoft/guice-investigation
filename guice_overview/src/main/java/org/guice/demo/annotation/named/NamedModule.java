package org.guice.demo.annotation.named;

import com.google.inject.*;

/**
 * User: dserdyuk
 * Date: 05.11.13
 */
public class NamedModule implements Module {

    private static Injector injector;

    public static Injector getInjector() {
        if (injector == null) {
            injector = Guice.createInjector(new NamedModule());
        }
        return injector;
    }

    @Override
    public void configure(Binder binder) {
        Goo first = new Goo("first");
        binder.bind(Goo.class).annotatedWith(new NamedImpl("f")).toInstance(first);
        Goo second = new Goo("second");
        binder.bind(Goo.class).annotatedWith(new NamedImpl("s")).toInstance(second);
    }
}
