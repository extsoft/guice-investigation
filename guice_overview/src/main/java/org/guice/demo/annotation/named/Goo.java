package org.guice.demo.annotation.named;

/**
 * User: dserdyuk
 * Date: 10.12.13
 */
public class Goo {

    private final String goo;

    public Goo(String goo) {
        this.goo = goo;
    }

    public String getGoo() {
        return goo;
    }
}
