package org.guice.demo.modules.copier;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public interface IReader {
    public String read();
}
