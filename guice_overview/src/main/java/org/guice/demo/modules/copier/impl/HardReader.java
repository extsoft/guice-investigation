package org.guice.demo.modules.copier.impl;

import org.guice.demo.modules.copier.IReader;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class HardReader implements IReader {

    public static final String SIMPLE_STRING = "HardString";

    public String read() {
        return SIMPLE_STRING;
    }
}
