package org.guice.demo.modules.copier.impl;

import org.guice.demo.modules.copier.IReader;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class SimpleReader implements IReader {

    public static final String SIMPLE_STRING = "SimpleString";

    public String read() {
        return SIMPLE_STRING;
    }
}
