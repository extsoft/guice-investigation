package org.guice.demo.modules.copier;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import org.guice.demo.modules.copier.impl.SimpleCopier;
import org.guice.demo.modules.copier.impl.SimpleReader;
import org.guice.demo.modules.copier.impl.SimpleWriter;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class SimpleCopyModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(IReader.class)
                .to(SimpleReader.class)
                .in(Scopes.SINGLETON);
        bind(IWriter.class)
                .to(SimpleWriter.class)
                .in(Scopes.SINGLETON);
        bind(ICopier.class)
                .to(SimpleCopier.class)
                .in(Scopes.SINGLETON);
    }
}
