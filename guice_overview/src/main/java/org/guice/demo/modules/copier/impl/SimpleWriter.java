package org.guice.demo.modules.copier.impl;

import org.guice.demo.modules.copier.IWriter;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class SimpleWriter implements IWriter {

    public void write(String data) {
        System.out.println(data);
    }
}