package org.guice.demo.modules.copier.impl;

import com.google.inject.Inject;
import org.guice.demo.modules.copier.ICopier;
import org.guice.demo.modules.copier.IReader;
import org.guice.demo.modules.copier.IWriter;

/**
 * User: dserdyuk
 * Date: 13.12.13
 */
public class SimpleCopier implements ICopier {

    private IReader reader;
    private IWriter writer;

    public SimpleCopier() {
    }

    @Inject
    public SimpleCopier(IWriter writer) {
        this.writer = writer;
    }

    public void copy() {
        writer.write(reader.read() + ". Made by SimpleCopier.");
    }

    @Inject
    public void setReader(IReader reader) {
        this.reader = reader;
    }
}
